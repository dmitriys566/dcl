#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "libdcl.h"

int n;

double f(double x,void *serviceData){
  return LegendreP(n,x);
}

double g(double x,void *serviceData){
  return x*x*x-3*x*x+2*x;
}

double f1(double x1[],void *serviceData){
  double x,y,z;
  x=x1[0];
  y=x1[1];
  z=x1[2];
  return x*x*x+x*sin(y*z);
}

double complex g1(double x1[],void *sd){
  return cexp(I*x1[0]);
}
void istart(){
  double sep,x0,x1;
  double *ksi;
  double *an;
  int i,nf;
  x0=0.0;
  x1=1.0;
  sep=1.0/(n*n);
  ksi=(double *)malloc(n*sizeof(double));
  an=(double *)malloc(n*sizeof(double));
  for(i=0;i<n;i++){
    ksi[i]=FindNZero(f,x0,x1,NULL,sep,i+1,&nf);
    an[i]=2.0/((1.0-ksi[i]*ksi[i])*(DLegendreP(n,ksi[i])*DLegendreP(n,ksi[i])));
//    printf("ksi[%d]=%.16lg\n",i,ksi[i]);
//    printf("an[%d]=%.16lg\n",i,an[i]);
  }
  printf("{");
  for(i=0;i<n;i++){
    printf("%.16lg,",ksi[i]);
  }
  printf("}\n");
  printf("{");
  for(i=0;i<n;i++){
    printf("%.16lg,",an[i]);
  }
  printf("}\n");
}

void F(int neq,double t,double * y,double rv[],void *serviceData){
  rv[0]=y[0]*y[0]+1;
}

int main(){
  int i,j;
  double x,y,t0,nsteps,dt;
  double complex p1,q1,res;
  double *a,*b,*h,alpha;
  double complex *w,*z;
  int N;
  N=4;
  a=(double *)malloc(N*N*sizeof(double));
  b=(double *)malloc(N*N*sizeof(double));
  h=(double *)malloc(N*N*sizeof(double));
  w=(double complex *)malloc(N*sizeof(double complex));
  z=(double complex *)malloc(N*N*sizeof(double complex));
  for(i=0;i<N;i++){
    for(j=0;j<=i;j++){
      a[i*N+j]=random_double();
      a[j*N+i]=a[i*N+j];
      b[i*N+j]=random_double();
      b[j*N+i]=b[i*N+j];
    }
  }
  alpha=0;
  while(alpha<=1.0){
    for(i=0;i<N;i++){
      for(j=0;j<N;j++){
        h[i*N+j]=alpha*a[i*N+j]+(1-alpha)*b[i*N+j];
      }
    }
    EigenSystem(N,h,w,z,0);
    printf("alpha=%.16lg\n",alpha);
    for(i=0;i<N;i++){
      printf("%.16lg\n",(double)w[i]);
    }
    alpha+=0.01;
  }
  return 0;
}