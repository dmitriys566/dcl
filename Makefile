all:
	gfortran -c libamos.f
	ar cru libamos.a libamos.o
	ranlib libamos.a
	gcc -c libdcl.c
	ar rc libdcl.a libdcl.o
	ranlib libdcl.a
	gcc -c libeispack.c
	ar rc libeispack.a libeispack.o
	ranlib libeispack.a
	gcc example.c -ldcl -lm -lamos -lgfortran -leispack -lcrypto -L. -o example
clean:
	rm -f *.o
	rm -f *.o
	rm -f *.a
	rm -f example